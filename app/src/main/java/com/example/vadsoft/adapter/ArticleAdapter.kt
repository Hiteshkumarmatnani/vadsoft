package com.example.vadsoft.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.vadsoft.R
import com.example.vadsoft.pojo.ArticleArray

class ArticleAdapter : RecyclerView.Adapter<ArticleAdapter.ViewHolder>() {

    lateinit var mContext: Context
    var articleArray: ArrayList<ArticleArray> = ArrayList()

    fun setArticleData(mContext: Context, articleArray: ArrayList<ArticleArray>){
        this.mContext = mContext
        this.articleArray = articleArray
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.article_adapter, parent, false))
    }

    override fun getItemCount(): Int {
        return articleArray.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.userName_txt.text = articleArray[position].articleUserName
        holder.articleCreateTime_txt.text = articleArray[position].articleCreateDate
        holder.userDesc_txt.text = articleArray[position].articleUserDesign
        holder.articleDesc_txt.text = articleArray[position].articleDescription
        holder.articleTitle_txt.text = articleArray[position].articleTitle
        holder.articleURL_txt.text = articleArray[position].articleURL
        holder.articleLike_txt.text = articleArray[position].articleLike + " Likes"
        holder.articleComment_txt.text = articleArray[position].articleComment +" Comments"

        holder.articleURL_txt.paintFlags = Paint.UNDERLINE_TEXT_FLAG

        if (articleArray[position].articleUserImage.isNotBlank()) {
            holder.userImage_img.visibility = View.VISIBLE
            Glide
                .with(mContext)
                .load(articleArray[position].articleUserImage)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .apply(RequestOptions().circleCrop())
                .placeholder(R.drawable.ic_launcher_background)
                .into(holder.userImage_img)
        } else {
            holder.userImage_img.visibility = View.GONE
        }

        if (articleArray[position].articleImage.isNotBlank()){
            holder.articleImage_img.visibility = View.VISIBLE
            Glide
                .with(mContext)
                .load(articleArray[position].articleImage)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.ic_launcher_background)
                .into(holder.articleImage_img)
        } else {
            holder.articleImage_img.visibility = View.GONE
        }

        holder.userInfoPanel_rel.setOnClickListener { v ->  onItemClickListener!!.
        onItemClickListener(articleArray[position].articleId.toString(), position)}

    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val userName_txt: TextView
        val userDesc_txt: TextView
        val articleCreateTime_txt: TextView
        val articleDesc_txt: TextView
        val articleTitle_txt: TextView
        val articleURL_txt: TextView
        val articleLike_txt: TextView
        val articleComment_txt: TextView
        val articleImage_img: ImageView
        val userImage_img: ImageView
        val userInfoPanel_rel: RelativeLayout
        init {
            userInfoPanel_rel       =   itemView.findViewById(R.id.userInfoPanel_rel)
            userImage_img           =   itemView.findViewById(R.id.userImage_img)
            userName_txt            =   itemView.findViewById(R.id.userName_txt)
            userDesc_txt            =   itemView.findViewById(R.id.userDesc_txt)
            articleCreateTime_txt   =   itemView.findViewById(R.id.articleCreateTime_txt)
            articleImage_img        =   itemView.findViewById(R.id.articleImage_img)
            articleDesc_txt         =   itemView.findViewById(R.id.articleDesc_txt)
            articleTitle_txt        =   itemView.findViewById(R.id.articleTitle_txt)
            articleURL_txt          =   itemView.findViewById(R.id.articleURL_txt)
            articleLike_txt         =   itemView.findViewById(R.id.articleLike_txt)
            articleComment_txt      =   itemView.findViewById(R.id.articleComment_txt)
        }
    }

    private var onItemClickListener: ItemClickListener? = null

    interface ItemClickListener {
        fun onItemClickListener(productId: String, position: Int)
    }

    fun set(clickListener: ItemClickListener) {
        onItemClickListener = clickListener
    }

}