package com.example.vadsoft.pojo

class ArticleArray {

    constructor(
        articleId: Int,
        articleUserImage: String,
        articleUserName: String,
        articleUserDesign: String,
        articleCreateDate: String,
        articleImage: String,
        articleTitle: String,
        articleDescription: String,
        articleURL: String,
        articleLike: String,
        articleComment: String
    ) {
        this.articleId = articleId
        this.articleUserImage = articleUserImage
        this.articleUserName = articleUserName
        this.articleUserDesign = articleUserDesign
        this.articleCreateDate = articleCreateDate
        this.articleImage = articleImage
        this.articleTitle = articleTitle
        this.articleDescription = articleDescription
        this.articleURL = articleURL
        this.articleLike = articleLike
        this.articleComment = articleComment
    }

    var articleId: Int = 0
    var articleUserImage: String
    var articleUserName: String
    var articleUserDesign: String
    var articleCreateDate: String
    var articleImage: String
    var articleTitle: String
    var articleDescription: String
    var articleURL: String
    var articleLike: String
    var articleComment: String = ""

}