package com.example.vadsoft.repository

import androidx.lifecycle.MutableLiveData
import com.example.vadsoft.pojo.ArticleArray

class ArticleRepo {

    private var instance: ArticleRepo? = null

    fun getInstance(): ArticleRepo? {
        if (instance == null){
            instance = this
        }

        return instance
    }

    var articleMutable: MutableLiveData<List<ArticleArray>> = MutableLiveData()

    fun fetchArticleFromServer(): MutableLiveData<List<ArticleArray>> {

        var articleArray = mutableListOf<ArticleArray>()

        articleArray.add(0, ArticleArray(1, "https://secure.gravatar.com/avatar/db0e9347f6a17a5b927f202d6c36bfc6?s=96&d=mm&r=g",
            "Rahul Mehta", "Writer", "2 Min ago", "https://www.successcds.net/images/article-writing-format.png",
            "The version control software market has evolved a lot since Bitbucket began in 2008.",
            "As we surpass 10 million registered users on the platform, we're at a point in our growth where we are conducting a deeper evaluation of the market and how we can best support our users going forward.",
            "https://bitbucket.org/blog/sunsetting-mercurial-support-in-bitbucket",
            "2", "6"))

        articleArray.add(1, ArticleArray(1, "https://secure.gravatar.com/avatar/db0e9347f6a17a5b927f202d6c36bfc6?s=96&d=mm&r=g",
            "Rahul Mehta", "Writer", "5 Hours ago", "",
            "The version control software market has evolved a lot since Bitbucket began in 2008.",
            "As we surpass 10 million registered users on the platform, we're at a point in our growth where we are conducting a deeper evaluation of the market and how we can best support our users going forward.",
            "https://bitbucket.org/blog/sunsetting-mercurial-support-in-bitbucket",
            "2", "6"))

        articleArray.add(2, ArticleArray(1, "https://secure.gravatar.com/avatar/db0e9347f6a17a5b927f202d6c36bfc6?s=96&d=mm&r=g",
            "Rahul Mehta", "Writer", "Yesterday", "",
            "The version control software market has evolved a lot since Bitbucket began in 2008.",
            "As we surpass 10 million registered users on the platform, we're at a point in our growth where we are conducting a deeper evaluation of the market and how we can best support our users going forward.",
            "https://bitbucket.org/blog/sunsetting-mercurial-support-in-bitbucket",
            "2", "6"))


        articleArray.add(3, ArticleArray(1, "https://secure.gravatar.com/avatar/db0e9347f6a17a5b927f202d6c36bfc6?s=96&d=mm&r=g",
            "Rahul Mehta", "Writer", "Yesterday", "",
            "The version control software market has evolved a lot since Bitbucket began in 2008.",
            "As we surpass 10 million registered users on the platform, we're at a point in our growth where we are conducting a deeper evaluation of the market and how we can best support our users going forward.",
            "https://bitbucket.org/blog/sunsetting-mercurial-support-in-bitbucket",
            "2", "6"))

        articleArray.add(4, ArticleArray(1, "https://secure.gravatar.com/avatar/db0e9347f6a17a5b927f202d6c36bfc6?s=96&d=mm&r=g",
            "Rahul Mehta", "Writer", "Yesterday", "",
            "The version control software market has evolved a lot since Bitbucket began in 2008.",
            "As we surpass 10 million registered users on the platform, we're at a point in our growth where we are conducting a deeper evaluation of the market and how we can best support our users going forward.",
            "https://bitbucket.org/blog/sunsetting-mercurial-support-in-bitbucket",
            "2", "6"))


        articleArray.add(4, ArticleArray(1, "https://secure.gravatar.com/avatar/db0e9347f6a17a5b927f202d6c36bfc6?s=96&d=mm&r=g",
            "Rahul Mehta", "", "29 June 2020", "https://www.successcds.net/images/article-writing-format.png",
            "The version control software market has evolved a lot since Bitbucket began in 2008.",
            "As we surpass 10 million registered users on the platform, we're at a point in our growth where we are conducting a deeper evaluation of the market and how we can best support our users going forward.",
            "https://bitbucket.org/blog/sunsetting-mercurial-support-in-bitbucket",
            "2", "6"))

        articleMutable.value = articleArray

        return articleMutable
    }
}