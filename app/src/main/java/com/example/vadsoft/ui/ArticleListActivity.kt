package com.example.vadsoft.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.vadsoft.R
import com.example.vadsoft.adapter.ArticleAdapter
import com.example.vadsoft.pojo.ArticleArray
import com.example.vadsoft.repository.ArticleRepo
import com.example.vadsoft.viewmodel.ArticleViewModel

class ArticleListActivity : AppCompatActivity() {

    var articleAdapter = ArticleAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.article_list_activity)

        val article_recyclerV  =   findViewById<RecyclerView>(R.id.article_recyclerV)

        article_recyclerV.layoutManager = LinearLayoutManager(this)

        article_recyclerV.adapter = articleAdapter

        val articleViewModel = ViewModelProvider(this).get(ArticleViewModel::class.java)

        articleViewModel.getAllArticle().observe(this, Observer<List<ArticleArray>>{
            t -> if (t != null){
            articleAdapter.setArticleData(applicationContext, t as ArrayList<ArticleArray>)
        }
        })

        articleAdapter.set(clickListener = object :
            ArticleAdapter.ItemClickListener {
            override fun onItemClickListener(productId: String, position: Int) {
                startActivity(Intent(applicationContext, UserDetailsActivity::class.java))
            }
        })

    }
}
