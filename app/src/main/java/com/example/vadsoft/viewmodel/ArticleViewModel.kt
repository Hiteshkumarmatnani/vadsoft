package com.example.vadsoft.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.vadsoft.pojo.ArticleArray
import com.example.vadsoft.repository.ArticleRepo

class ArticleViewModel(application: Application) : AndroidViewModel(application) {

    var articleRepo: ArticleRepo

    init {
        articleRepo = ArticleRepo().getInstance()!!
    }

    fun getAllArticle(): LiveData<List<ArticleArray>> {
        return articleRepo.fetchArticleFromServer()
    }

}